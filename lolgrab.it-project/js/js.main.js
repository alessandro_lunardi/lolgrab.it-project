/**************** Template ****************/
/*STATIC VARS*/
var replaceChars={ "%20":" " , "%27":"'" };
/*END OF STATIC VARS*/

var source = document.getElementById("info-champ-template").innerHTML;
var template = Handlebars.compile(source);
  
/**
 * This function search in the page URL the ID of the champion
 * EX: http://lolgrab.it/mypagechampion.html?id=aatrox
 * The ID in this case is aatrox
 */
function getUrlVars() {
var vars = {};
var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
vars[key] = value;
});
return vars;
}
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
/**
 * This function search and return the champion object
 * It call getUrlVars() function.
 */
function championNameFinder(){
    var id = (getUrlVars()["campione"]).replace(/%20|%27/g,function(match) {return replaceChars[match];});
    var champObj = data1.champions[0][id.capitalize()];
    
    return champObj;
}

Handlebars.registerHelper('championSelect', function (options){
    var cObj = options.fn(championNameFinder());
    return cObj;
});

    var output = template(data1);
    document.getElementById("main-container").innerHTML = output;
/************PAGE**********************************************/
$(document).ready(function (){
    textTooltip();
    getSkins();
    /*getSpellsImg();
    getSpellsTitle();
    getSpellsDescription();*/
});

function textTooltip(){
    var contenuto = "<strong>Salute:<br></strong>" + championNameFinder().stats.hp +" (+" + championNameFinder().stats.hpperlevel + " per livello)<br><br>" + "<strong>Attacco fisico:<br></strong>" + championNameFinder().stats.attackdamage+" (+" + championNameFinder().stats.attackdamageperlevel+ " per livello)<br><br>"+"<strong>Velocità d'attacco:<br></strong>"+" (+" +championNameFinder().stats.attackspeedperlevel+" % per livello)<br><br>" +"<strong>Velocità di movimento:<br></strong>"+championNameFinder().stats.movespeed+"<br><br>"+ "<strong>Rigen. salute:<br></strong>"+championNameFinder().stats.hpregen+" (+"+championNameFinder().stats.hpregenperlevel+" per livello)<br><br>"+"<strong>Armatura:<br></strong>"+championNameFinder().stats.armor+" (+"+ championNameFinder().stats.armorperlevel+" per livello)<br><br>"+"<strong>Resist. magica:<br></strong>"+championNameFinder().stats.spellblock+" (+"+championNameFinder().stats.spellblockperlevel+" per livello)<br><br>"+"<strong>Gittata:<br></strong>"+championNameFinder().stats.attackrange;
$("p.tooltip-stat").qtip({
    content:  contenuto,
    position: {
             target: 'mouse', 
             adjust: { x: 20, y: 20 } 
         },
    style: {
           classes: 'tooltip-style',
           widget: true, 
           def: false 
    }
  }); 
}
/****************Skin**************************/
function getSkins(){
    var images = 'http://ddragon.leagueoflegends.com/cdn/img/champion/splash/'
    var out = "";
        for(var i=0;i<championNameFinder().skins.length;i++){
            var imgTag = document.createElement("img");
            var figureTag = document.createElement("figure");
            var figcaptionTag = document.createElement("figcaption");
            var h3Tag = document.createElement("h3");
            figureTag.className = "figura";
            h3Tag.className="skin-name";
                out = images + championNameFinder().key + "_"+championNameFinder().skins[i].num+".jpg";
                imgTag.className = "skin-img";
                imgTag.setAttribute("alt", "champ");
                imgTag.setAttribute("src", out);
            if(i==0){h3Tag.textContent=championNameFinder().name;}
            else{h3Tag.textContent = championNameFinder().skins[i].name;}
            figcaptionTag.appendChild(h3Tag);
            figureTag.appendChild(imgTag);
            figureTag.appendChild(figcaptionTag);
            document.getElementById("div-cont-skin").appendChild(figureTag);
        }
}
/******************Spell*********************************/
function getSpells(){
    /* CONSTANTS */
    var link_img_spells="http://ddragon.leagueoflegends.com/cdn/6.24.1/img/";
    var link_video_spells= "https://lolstatic-a.akamaihd.net/champion-abilities/videos/";
    var link = "";
    var i = 0;
    /* Get Spells Images */
    var div_colonna_65 = document.createElement("div");
        div_colonna_65.className = "colonna-65 prova";
    var div_spell_img_container = document.createElement("div");
        div_spell_img_container.className = "spell-img-container";
    var div_spell_title_container = document.createElement("div");
        div_spell_title_container.className = "spell-title-container";
    var p_spell_title = document.createElement("p");
        p_spell_title.className = "spell-title";
    var div_spell_description_container = document.createElement("div");
        div_spell_description_container.className = "spell-description-container";
    var div_colonna_35 = document.createElement("div");
        div_colonna_35.className = "colonna-35 prova video-container";
    var video_tag = document.createElement("video");
        video_tag.className = "video-spell";
    var spell_img = document.createElement("img");
        spell_img.className = "spell-img";
    var spell_title = document.createElement("p");
        spell_title.className = "spell-title";

    /* TO-DO */

    for(i=0; i<(championNameFinder().spells.length+1); i++){
        if(i==0){
            var img_spell = document.createElement("img");
            img_spell.className="spell-img";
            link = link_img_spells+"passive/" +championNameFinder().passive.image.full;
            img_spell.setAttribute("src",link);
        }
    }
}

function getSpellsImg(){
    var div_cont_img_spell = document.getElementById("spell-img");
    var link_img_spells="http://ddragon.leagueoflegends.com/cdn/6.24.1/img/";
    var link='';
    var j=0;
    for(var i=0;i<championNameFinder().spells.length+1;i++){
        if(i==0){
            var img_spell = document.createElement("img");
            img_spell.className="spell-img";
            link = link_img_spells+"passive/" +championNameFinder().passive.image.full;
            img_spell.setAttribute("src",link);
        }else if(i>0){
            var img_spell = document.createElement("img");
            img_spell.className="spell-img";
            link = link_img_spells+"spell/"+ championNameFinder().spells[j].image.full;
            img_spell.setAttribute("src",link);
            j++;
        }
        div_cont_img_spell.appendChild(img_spell);
    }
}

function getSpellsTitle(){
    var p_title = document.getElementById("s-title-container");
    var j=0;
    for(var i=0;i<championNameFinder().spells.length+1;i++){
        if(i==0){//primo ciclo carica la passiva
            var p_title_text = document.createElement("p");
            p_title_text.className="spell-title";
            var text = championNameFinder().passive.name;
            p_title_text.innerHTML=text;
        }else if(i>0){//dal secondo in poi carica le altre spells
            var p_title_text = document.createElement("p");
            p_title_text.className="spell-title";
            var text = championNameFinder().spells[j].name;
            p_title_text.innerHTML=text;
            j++;
        }
        p_title.appendChild(p_title_text);
    }
}
function getSpellsDescription(){
    var p_description = document.getElementById("s-description-container");
    var j=0;
    for(var i=0;i<championNameFinder().spells.length+1;i++){
        if(i==0){//primo ciclo carica la passiva
            var p_description_text = document.createElement("p");
            p_description_text.className="spell-description-container";
            var text = championNameFinder().passive.description;
            p_description_text.innerHTML=text;
        }else if(i>0){//dal secondo in poi carica le altre spells
            var p_description_text = document.createElement("p");
            p_description_text.className="spell-description-container";
            var text = championNameFinder().spells[j].description;
            p_description_text.innerHTML=text;
            j++;
        }
        p_description.appendChild(p_description_text);
    }
}

function getPassiveVideo(){
    var link_video = "https://lolstatic-a.akamaihd.net/champion-abilities/videos/";
    var video_passive = document.getElementById("video-passive");
    
    if(Modernizr.video && Modernizr.video.webm) {
        if((championNameFinder().id<=99) && (championNameFinder().id>=10)){
            link = link_video +"webm/00"+ championNameFinder().id +"_01.webm";
            video_passive.setAttribute("src", link);
        }else if(championNameFinder().id>=100){
        link = link_video +"webm/0"+ championNameFinder().id +"_01.webm";
        video_passive.setAttribute("src", link);
        }else if((championNameFinder().id>=0) && (championNameFinder().id<=9)){
        link = link_video +"webm/000"+ championNameFinder().id +"_01.webm";
        video_passive.setAttribute("src", link); 
        }
    } else if(Modernizr.video && Modernizr.video.ogg) {
        if((championNameFinder().id<=99) && (championNameFinder().id>=10)){
        link = link_video +"ogv/00"+ championNameFinder().id +"_01.ogv";
        video_passive.setAttribute("src", link);
        }else if(championNameFinder().id>=100){
        link = link_video +"ogv/0"+ championNameFinder().id +"_01.ogv";
        video_passive.setAttribute("src", link);
        }else if((championNameFinder().id>=0) && (championNameFinder().id<=9)){
        link = link_video +"ogv/000"+ championNameFinder().id +"_01.ogv";
        video_passive.setAttribute("src", link);
        }
    } else if(Modernizr.video && Modernizr.video.h264) {
        if((championNameFinder().id<=99) && (championNameFinder().id>=10)){
        link = link_video +"mp4/00"+ championNameFinder().id +"_01.mp4";
        video_passive.setAttribute("src",link);
        }else if(championNameFinder().id>=100){
        link = link_video +"mp4/0"+ championNameFinder().id +"_01.mp4";
        video_passive.setAttribute("src",link);
        }else if((championNameFinder().id>=0) && (championNameFinder().id<=9)){
        link = link_video +"mp4/000"+ championNameFinder().id +"_01.mp4";
        video_passive.setAttribute("src", link);
        }
    }

    video_passive.load();
    
}

function getSpellsVideo(){
    var link_video = "https://lolstatic-a.akamaihd.net/champion-abilities/videos/";
    var video_container = document.getElementById("video-container");
    var count = 2;
    for(var i=0;i<championNameFinder().spells.length;i++){
        var video = document.createElement("video");
        video.controls= "controls";
    if(Modernizr.video && Modernizr.video.webm) {
        if((championNameFinder().id<=99) && (championNameFinder().id>=10)){
            link = link_video +"webm/00"+ championNameFinder().id +"_0"+count+".webm";
      video.setAttribute("src", link);
        }else if(championNameFinder().id>=100){
        link = link_video +"webm/0"+ championNameFinder().id +"_0"+count+".webm";
      video.setAttribute("src", link);
        }else if((championNameFinder().id>=0) && (championNameFinder().id<=9)){
        link = link_video +"webm/000"+ championNameFinder().id +"_0"+count+".webm";
      video.setAttribute("src", link); 
        } 
    } else if(Modernizr.video && Modernizr.video.ogg) {
       if((championNameFinder().id<=99) && (championNameFinder().id>=10)){
        link = link_video +"ogv/00"+ championNameFinder().id +"_0"+count+".ogv";
        video.setAttribute("src", link);
        }else if(championNameFinder().id>=100){
        link = link_video +"ogv/0"+ championNameFinder().id +"_0"+count+".ogv";
        video.setAttribute("src", link);
        }else if((championNameFinder().id>=0) && (championNameFinder().id<=9)){
        link = link_video +"ogv/00"+ championNameFinder().id +"_0"+count+".ogv";
        video.setAttribute("src", link);
        } 
    } else if(Modernizr.video && Modernizr.video.h264) {
        if((championNameFinder().id<=99) && (championNameFinder().id>=10)){
        link = link_video +"mp4/00"+ championNameFinder().id +"_0"+count+".mp4";
        video_passive.setAttribute("src",link);
        }else if(championNameFinder().id>=100){
        link = link_video +"mp4/0"+ championNameFinder().id +"_0"+count+".mp4";
        video_passive.setAttribute("src",link);
        }else if((championNameFinder().id>=0) && (championNameFinder().id<=9)){
        link = link_video +"mp4/000"+ championNameFinder().id +"_0"+count+".mp4";
        video_passive.setAttribute("src", link);
        }
    }
        
        video_container.appendChild(video);
        video.load();
        count ++; 
    }
    
}
